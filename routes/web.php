<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomePageController@index');
Route::get('our-room/{slug}', 'HomePageController@room')->name('our-room');
Route::get('/book-room', 'HomePageController@bookRoom')->name('book-room');

Route::get('/testimonies', 'TestimonyController@index');
Route::post('/testimonies', 'TestimonyController@store');
Route::get('/testimonies/new', function () {
	return view('testing.testimony.form');
});
Route::get('/testimonies/{testimonyId}/edit', 'TestimonyController@edit');
Route::post('/testimonies/{testimonyId}/edit', 'TestimonyController@update');
Route::post('/testimonies/{testimonyId}/delete', 'TestimonyController@delete');

Route::get('/facilities', 'FacilityController@index');
Route::post('/facilities', 'FacilityController@store');
Route::get('/facilities/new', function () {
	return view('testing.facility.form');
});
Route::get('/facilities/{facilityId}/edit', 'FacilityController@edit');
Route::post('/facilities/{facilityId}/edit', 'FacilityController@update');
Route::post('/facilities/{facilityId}/delete', 'FacilityController@delete');

Route::get('/config/header', 'ConfigurationController@editHeaderSection');
Route::put('/config/header', 'ConfigurationController@updateHeaderSection');

Route::get('/config/service', 'ConfigurationController@editServiceSection');
Route::put('/config/service', 'ConfigurationController@updateServiceSection');

Route::get('/config/room', 'ConfigurationController@editRoomSection');
Route::put('/config/room', 'ConfigurationController@updateRoomSection');

Route::get('/config/general', 'ConfigurationController@editGeneralSection');
Route::put('/config/general', 'ConfigurationController@updateGeneralSection');

Route::get('/config/about', 'ConfigurationController@editAboutSection');
Route::put('/config/about', 'ConfigurationController@updateAboutSection');

Route::post('/media', 'ConfigurationController@addMedia');
Route::delete('/media/{mediaId}', 'ConfigurationController@destroyMedia');

// room route
Route::resource('/rooms','RoomController');
Route::get('rooms/room-images/{id}', 'RoomController@formRemoveImage');

Route::delete('/rooms/room-images/{$id}', 'RoomController@removeImage');
Route::resource('room-facility', 'RoomFacilityController', ['except' => 'show']);


Route::get('/home', 'HomeController@index')->name('home');

Route::group([
	'namespace' => 'Auth'
],
	function () {

		// Authentication Routes...
		$this->get('login', 'LoginController@showLoginForm')->name('login');
		$this->post('login', 'LoginController@login');
		$this->post('logout', 'LoginController@logout')->name('logout');

	});
