@extends('layouts.app', [
  'title' => 'Sunset Point Hotel'])

@section('content')

    <section id="room-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 wow fadeInLeft animation-delay-3">
                    <div class="slide">
                        <ul id="lightSlider">
                            @foreach($room->images as $media)
                                <li data-thumb="{{ asset($media->thumbs) }}">
                                    <img src="{{ asset($media->url) }}" />
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="content">

                        <div class="name-price d-flex align-items-center" style="margin-bottom: 8px;">
                            <h1 class="title">{{ $room->name }}</h1>
                            <div class="price">
                                <p>Start from <span>{{ $room->harga }}</span> / night</p>
                            </div>
                        </div>
                        <hr>

                        <div class="room-facility">
                            <ul class="list-inline">
                                @foreach($room->room_facilities as $index => $facility)
                                    <li class="list-inline-item wow fadeInDown animation-delay-{{ $index+1 }}">
                                        <img src="{{ asset($facility->image) }}" alt="{{ $facility->name }}" />{{ $facility->name }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="room-details wow fadeInUp animation-delay-5">
                            <h2 class="details">Rooms Details </h2>
                            {!! $room->detail !!}
                        </div>

                    </div>
                </div>
                <div class="sidebar col-md-4 wow fadeInRight">
                    <div class="card-header title ">More Our Room</div>
                    <ul class="card list-unstyled">
                        @foreach($latests as $index => $latest)
                            <li class="card-body media wow fadeInDown animation-delay-{{ $index+1 }}">
                                @if(count($latest->images))
                                <a href="{{ route('our-room', $latest->slug) }}">
                                    <img height="40px" class="mr-3" src="{{ app('url')->asset($latest->images->first()->thumbs) }}" alt="{{ $latest->name }}">
                                </a>
                                @endif

                                <div class="media-body">
                                    <a href="{{ route('our-room', $latest->slug) }}">
                                        <h5 class="text-dark font-weight-bold mt-0 mb-1">{{ $latest->name }}</h5>
                                        <p class="text-muted">Start from {{ $latest->harga }} / night</p>
                                    </a>
                                    <hr>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div>
                        <a href={{route('book-room')}} class="btn btn-block btn-custom" >Review</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection