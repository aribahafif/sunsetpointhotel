@extends('layouts.admin', [
'title' => 'Room',
'url' => route('room-facility.create')
])

@section('content')

    <div class="card-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($facilities as $index => $facility)
                <tr>
                    <td>{{ $index+1 }}</td>
                    <td>{{ $facility->name }}</td>
                    <td><img height="30px" src="{{ url($facility->image) }}"/></td>
                    <td class="btn-group" role="group">
                        <a class="btn btn-sm btn-warning" href="{{ route('room-facility.edit', $facility->id) }}" data-tooltip="tooltip" title="Edit">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a href="{{ route('room-facility.destroy', $facility->id) }}" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete"  data-tooltip="tooltip" title="Delete">
                            <i class="fa fa-trash"></i>
                        </a>

                        <!-- Modal -->
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete room</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure delete room facility {{ $facility->name }}?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="{{ route('room-facility.destroy', $facility->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('js/datatable.js') }}"></script>
@endpush