@extends('layouts.admin', [
'title' => isset($facility) ? 'Update room facility' .$facility->name : 'New room facility'
])

@section('content')
    <div class="card-body">
        <form method="POST" class="needs-validation" novalidate action="{{ isset($facility) ? url('room-facility/'.$facility->id)  : url('room-facility') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            @if(isset($facility))
                <input type="hidden" name="_method" value="PUT">
            @endif

            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input class="form-control" type="text" name="name" value="{{ isset($facility) ? $facility->name : '' }}" placeholder="Name" required>

                <div class="valid-feedback">
                    Looks good!
                </div>

                <div class="invalid-feedback">
                    Please input a name
                </div>
            </div>


            @if(isset($facility))
                <img class="img-fluid img-thumbnail" src="{{ url($facility->image) }}"/>
            @endif

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" {{ isset($facility) ?: 'required' }} name="image" accept="image/*" />
                    <label class="custom-file-label" for="image">Choose file</label>
                    <p class="small">Please input your image with hd 1024x768 </p>
                </div>

                <div class="valid-feedback">
                    Looks good!
                </div>

                <div class="invalid-feedback">
                    Please input your image with hd 1024x76
                </div>

            </div>



            <div class="pt-5 d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-save"></i>
                    {{ isset($facility) ? 'Update' : 'Save' }}
                </button>
            </div>


        </form>
    </div>
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/editor.js') }}"></script>
@endpush