<div class="modal-header">
    <h5 class="modal-title" id="myModalLabel">Delete Image</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
    Are you sure delete this images?
</div>
<div class="modal-footer">
   <form method="POST" action="{{ url('/rooms/room-images/'.$image->id) }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-danger">Delete</button>
</form>
</div>