@extends('layouts.admin', [
	'title' => isset($room) ? 'Update room ' .$room->name : 'New room',
	'show' => isset($room) ? route('our-room', $room->slug): null
	])

	@section('content')
	<div class="card-body">
		<form method="POST" class="needs-validation" novalidate action="{{ isset($room) ? url('rooms/'.$room->id)  : url('rooms') }}" enctype="multipart/form-data">
			{{ csrf_field() }}

			@if(isset($room))
			<input type="hidden" name="_method" value="put">
			@endif

			<div class="row">
				<div class="col-lg-8">
					<div class="form-group">
						<label for="name" class="control-label">Name</label>
						<input class="form-control" type="text" name="name" value="{{ isset($room) ? $room->name : '' }}" placeholder="Name" required>

						<div class="valid-feedback">
							Looks good!
						</div>

						<div class="invalid-feedback">
							Please input a name
						</div>
					</div>



					<div class="form-group">
						<label for="detail" class="control-label">Detail</label>
						<textarea class="form-control" id="content" name="detail">{{ isset($room) ? $room->detail : '' }}</textarea>
					</div>

					
					@if(isset($room))
					<div class="form-group">
						<div class="row">
							@foreach($room->images as $image)
							<div class="col-md-3 pt-3 pb-3">
								<img src="{{ asset($image->thumbs) }}" alt="" class="img-fluid">
								{{-- <div data-image="{{ $image->id }}" class="btn btn-danger btn-block btn-sm remove-image-item">
									<i class="fa fa-trash-o"></i>
								Delete</div> --}}
								<!-- Link trigger modal -->
<a href="{{ url('rooms/room-images/' . $image->id) }}" data-remote="false" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-danger btn-block">
    <i class="fa fa-trash-o"></i>
								Delete
</a>
							</div>
							@endforeach
						</div>
					</div>
					@endif
					<div class="form-group">
						<label for="image">Images</label>
						<input type="file" name="images[]" id="images" class="form-control" multiple>
						<span class="text-warning">You can use ctr (cmd) to select multiple images</span>
					</div>

				</div>

				<div class="col-lg-4">
					<div class="form-group">
						<label for="price" class="control-label">Price</label>
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<div class="input-group-text">Rp</div>
							</div>
							<input type="text" class="form-control" id="price" name="price" value="{{ isset($room) ? $room->price : '' }}" required>

						</div>

						<div class="valid-feedback">
							Looks good!
						</div>

						<div class="invalid-feedback">
							Please input a price
						</div>

					</div>

					<div class="form-group">
						<label class="control-label" for="room-facility"> Room Facility</label>
						<select name="facilities[]" id="facilities" class="custom-select" multiple>
							@foreach ($allFacilities as $facility)
							<option
							{{--@if (in_array($facility, $allFacilities)) selected @endif--}}
							value="{{ $facility->id }}" {{ in_array($facility->id, $facilities) ? 'selected' : '' }}>
							{{ $facility->name }}
						</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="status" class="control-label">Status</label>
					<select class="custom-select" name="status" value="{{ isset($room) ? $room->status : '' }}">
						<option value="published">Published</option>
						<option value="unpublished">Unpublished</option>
					</select>
				</div>

			</div>
		</div>



		<div class="pt-5 d-flex justify-content-center">
			<button type="submit" class="btn btn-danger">
				<i class="fa fa-save"></i>
				{{ isset($room) ? 'Update' : 'Save' }}
			</button>
		</div>

	</form>
</div>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
    </div>
  </div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')

<script src="{{ asset('js/editor.js') }}"></script>
<script src="{{ asset('js/inputmask.js') }}"></script>
<script type="text/javascript">
	// Fill modal with content from link href
$("#myModal").on("show.bs.modal", function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>
{{-- <script>
	$("body").on("click",".remove-image-item",function() {
		var id = $(this).data('image');
		console.log(id);
		$('form#delete-image-'+id).submit();
	});
</script> --}}
@endpush