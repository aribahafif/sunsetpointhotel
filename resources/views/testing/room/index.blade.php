@extends('layouts.admin', [
'title' => 'Room',
'url' => url('rooms/create')
])

@section('content')

    <div class="card-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Details</th>
                <th>Facilities</th>
                <th>Cover</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($rooms as $index => $val)
                <tr>
                    <td>{{ ++$index }}</td>
                    <td>{{ $val->name }}</td>
                    <td>{{ $val->harga }}</td>
                    <td>{!! $val->excerpt !!}</td>
                    <th>
                        @foreach($val->room_facilities as $facility)
                            <span class="badge badge-info">
                                {{ $facility->name }}
                            </span>
                            @endforeach
                    </th>
                    <td>
                        @if(count($val->images))
                        <img class="img-fluid" src="{{ app('url')->asset($val->images->first()->thumbs) }}"/>
                        @endif
                    </td>
                    <td class="btn-group" role="group">
                        <a class="btn btn-sm btn-warning" href="{{ url('rooms/'.$val->id.'/edit') }}" data-tooltip="tooltip" title="Edit">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a class="btn btn-sm btn-success" href="{{ route('our-room', $val->slug) }}" data-tooltip="tooltip" title="show {{ $val->name }}">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ url('rooms/'.$val->id.'/edit') }}" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#published" data-tooltip="tooltip" title="{{ $val->status == 'published' ? 'unpublished' : 'published' }}">
                            <i class="fa fa-{!! $val->status == 'published' ? 'check' : 'times' !!}"></i>

                        </a>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete"  data-tooltip="tooltip" title="Delete">
                            <i class="fa fa-trash"></i>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="published" tabindex="-1" role="dialog" aria-labelledby="changeStatusModal" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="changeStatusModal">Change status</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to {{ $val->status == 'published' ? 'Unpublished' : 'Published' }} ?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" action="{{ url('rooms/'.$val->id.'/edit') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="status" value="{{ $val->status == 'published' ? 'unpublished' : 'published' }}">
                                            {{-- <input type="submit" value="{{ $val->status == 'published' ? 'Unpublished' : 'Published' }}"> --}}
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-times"></i>
                                            Close</button>
                                            <button type="submit" class="btn btn-warning">{{ $val->status == 'published' ? 'Unpublished' : 'Published' }}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- Modal -->
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="deleteRoomModal" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteRoomModal">Delete room</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure delete room?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="{{ url('rooms/'.$val->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('js/datatable.js') }}"></script>
@endpush
