@extends('layouts.admin', [
'title' => 'Testimonies',
'url' => url('testimonies/new')
])

@section('content')
    <div class="card-body">
        <table id="testimonies" class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Content</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($testimonies as $index => $testimony)
                <tr>
                    <td>{{ ++$index }}</td>
                    <td>{{ $testimony->content }}</td>
                    <td>{{ $testimony->name }}</td>
                    <td class="btn-group" role="group">

                        <a href="{{ url('testimonies/'.$testimony->id.'/edit') }}" class="btn btn-sm btn-warning" data-tooltip="tooltip" data-placement="top" title="Edit">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a href="{{ url('testimonies/'.$testimony->id.'/edit') }}" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#published" data-tooltip="tooltip" data-placement="top" title="{{ $testimony->status == 'published' ? 'unpublished' : 'published' }}">
                            <i class="fa fa-{{ $testimony->status == 'published' ? 'check' : 'times' }}"></i>
                        </a>

                        <a href="{{ url('testimonies/'.$testimony->id.'/delete') }}" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete" data-tooltip="tooltip" data-placement="top" title="Delete">
                            <i class="fa fa-trash"></i>

                        </a>

                        <!-- Modal -->
                        <div class="modal fade" id="published" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Change status</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to {{ $testimony->status == 'published' ? 'Unpublished' : 'Published' }} ?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" action="{{ url('testimonies/'.$testimony->id.'/edit') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="status" value="{{ $testimony->status == 'published' ? 'unpublished' : 'published' }}">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">{{ $testimony->status == 'published' ? 'Unpublished' : 'Published' }}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete testimony</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure delete from {{ $testimony->name }}?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" action="{{ url('testimonies/'.$testimony->id.'/delete') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
<script src="{{ asset('js/datatable.js') }}"></script>
@endpush