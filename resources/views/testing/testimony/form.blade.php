@extends('layouts.admin', [
'title' => isset($testimony) ? 'Update testimony ' .$testimony->name : 'New testimony'
])

@section('content')

    <div class="card-body">
        <form method="post" class="needs-validation" novalidate action="{{ isset($testimony) ? url("testimonies/{$testimony->id}/edit") : url("testimonies") }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="labe" class="control-label">Content</label>
                <textarea class="form-control" id="content" name="content" cols="30" rows="10" required>{{ isset($testimony) ? $testimony->content : '' }}</textarea>

                <div class="valid-feedback">
                    Looks good!
                </div>

                <div class="invalid-feedback">
                    Please input a content
                </div>

            </div>

            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" type="text" name="name" value="{{ isset($testimony) ? $testimony->name : '' }}" placeholder="Name" required>

                <div class="valid-feedback">
                    Looks good!
                </div>

                <div class="invalid-feedback">
                    Please input a description
                </div>

            </div>

            <div class="form-group">
                <label for="status" class="control-label">Status</label>
                <select class="custom-select" name="status" value="{{ isset($testimony) ? $testimony->status : '' }}">
                    <option value="published">Published</option>
                    <option value="unpublished">Unpublished</option>
                </select>
            </div>

            <div class="pt-5 d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-save"></i>
                    {{ isset($testimony) ? 'Update' : 'Save' }}
                </button>
            </div>

        </form>
    </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/editor.js') }}"></script>
@endpush