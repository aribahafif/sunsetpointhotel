@extends('layouts.admin', [
'title' => 'Room Setting'
])

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ url('/config/room') }}">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            <div class="form-group title">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $room->title }}" placeholder="Please enter title..." required>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a title </div>
            </div>

            <div class="form-group">
                <label for="subtitle">Subtitle</label>
                <textarea name="subtitle" id="content subtitle" class="form-control" placeholder="Please enter subtitle...">
                        {{ $room->subtitle }}
                    </textarea>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a subtitle </div>
            </div>

            <div class="d-flex justify-content-center">
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
            </div>
        </form>
    </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/editor.js') }}"></script>
@endpush