<!-- Modal -->
<div class="modal fade" id="delete-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete this image?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img id="delete-preview" src="" class="img-fluid">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          <i class="fa fa-close"></i>
          Close</button>

        <button type="button" class="btn btn-danger" id="confirm-delete" data-form="">
          <i class="fa fa-trash"></i>
          Delete</button>
      </div>
    </div>
  </div>
</div>