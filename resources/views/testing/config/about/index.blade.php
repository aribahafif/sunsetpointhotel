@extends('layouts.admin', [
'title' => 'About Setting'
])

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ url('/config/about') }}" id="aboutForm">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ $about->title }}" required placeholder="Please enter title...">
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" id="content" class="form-control" placeholder="Please enter content...">
                    {{ $about->content }}
                </textarea>
            </div>

            <div class="d-flex justify-content-center">
                <div class="btn btn-danger" onclick="document.getElementById('aboutForm').submit()">
                    <i class="fa fa-save"></i>
                    Save
                </div>
            </div>
        </form>

        <div id="about-gallery" class="row cards-about-gallery pt-5">
            @foreach($media as $index => $eachMedia)
                <div class="col-md-6 col-lg-4  wow bounceInUp animation-delay-{{ ++$index * 2 }}">
                    <div class="card transform-on-hover">
                        <a class="lightbox" href="#">
                            <img src="{{ url($eachMedia->url) }}" alt="Card Image" class="card-img-top">
                        </a>
                        <div class="card-body text-center">
                            <p>{{ $eachMedia->name }}</p>
                            <p>Added: {{ $eachMedia->created_at->format('M j, Y') }}</p>
                            {{--<a class="badge badge-info"><i class="fa fa-eye"></i></a>--}}
                            <a class="badge badge-danger image-remove" href="#" data-tooltip="tooltip" title="Delete Image" data-toggle="modal" data-target="#delete-image" data-url="{{ url($eachMedia->url) }}" data-form="{{ '#image-'.$eachMedia->id }}">
                                <i class="fa fa-trash"></i>
                            </a>
                            <form id="{{ 'image-'.$eachMedia->id }}" action="{{ url('/media/'.$eachMedia->id) }}" method="POST">
                                <input type="hidden" name="_method" value="delete" />
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="d-flex justify-content-center">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload-image">
            <i class="fa fa-image"></i>
            Add Image
        </button>
    </div>
    </div>

    @include('testing.config.about.add')
    @include('testing.config.about.delete')
    @include('testing.config.about.preview')
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/editor.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".image-preview").on('click', function () {
                $('#image-preview').attr('src', $(this).data('url'))
                $("#image-preview-modal").modal();
            });

            $(".image-remove").on('click', function () {
                $('#delete-preview').attr('src', $(this).data('url'))
                $('#confirm-delete').data('form', $(this).data('form'))
            });

            $('#confirm-delete').on('click', function (event) {
                event.preventDefault()
                $($(this).data('form')).submit()
            })

            $("#image").on('change', function (event) {
                var placeholder = $("#imageLabel").data('placeholder')

                if (event.target.files.length) {
                    placeholder = '';

                    for (var index = 0; index < event.target.files.length; index++) {
                        if (index > 0) {
                            placeholder += ", ";
                        }

                        placeholder += event.target.files[index].name
                    }
                }

                $("#imageLabel").html(placeholder)
            })
        })
    </script>
@endpush
