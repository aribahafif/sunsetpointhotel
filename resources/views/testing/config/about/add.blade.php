<!-- Modal -->
<div class="modal fade" id="upload-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="imageForm" method="POST" action="{{ url('media') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="sr-only" for="image">Upload image</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
              <label class="custom-file-label" for="image" id="imageLabel" data-placeholder="Choose image...">Choose image...</label>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <div class="btn btn-secondary" data-dismiss="modal">
            <i class="fa fa-close"></i>
            Close
          </div>

          <div class="btn btn-primary" onclick="document.getElementById('imageForm').submit()">
            <i class="fa fa-save"></i>
            Save
          </div>
        </div>
      </form>
    </div>
  </div>
</div>