@extends('layouts.admin', [
'title' => 'General Setting'
])

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ url('/config/general') }}" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}

            <h3 class="mb-2">Logo</h3>

            @if(!empty($general->logo))
                <img src="{{ url($general->logo) }}" class="img-fluid mx-auto mb-2">
            @endif

            <div class="form-group">
                <label class="" for="logo">Logo</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="logo" name="logo" accept="image/*">
                    <label class="custom-file-label" id="logoLabel" for="logo" data-placeholder="Choose logo...">
                        Choose logo...
                    </label>
                </div>
            </div>

            <h3>Contact</h3>
            <div class="form-group">
                <label for="phone" class="col-form-label">Phone</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-phone" id="phone"></span>
                    </div>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $general->phone }}" placeholder="Input phone...">
                    <div class="invalid-feedback">
                        Please input phone.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-form-label">Email</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-envelope" id="email"></span>
                    </div>
                    <input type="text" class="form-control" id="email" name="email"  placeholder="Input email..."  value="{{ $general->email }}">
                    <div class="invalid-feedback">
                        Please input email.
                    </div>
                </div>
            </div>

            <h3 class="mb-2">Location</h3>
            <div class="form-group">
                <textarea name="location" id="subtitle" class="form-control" placeholder="Please enter location..." required>
                    {{ $general->location }}
                </textarea>

                <div class="invalid-feedback">
                    Please input location.
                </div>
            </div>

            <h3 class="mb-2">Social media</h3>
            <div class="form-group">
                <label for="facebook" class="sr-only">Facebook</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="facebook">https://facebook.com/</span>
                    </div>
                    <input type="text" class="form-control" id="facebook" name="facebook_user" value="{{ $general->facebook_user }}" placeholder="Username">
                    <div class="invalid-feedback">
                        Please input facebook username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="facebook" class="sr-only">Twitter</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="twitter">https://twitter.com/</span>
                    </div>
                    <input type="text" class="form-control" id="twitter" name="twitter_user"  value="{{ $general->twitter_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input twitter username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="youtube" class="sr-only">Youtube</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="youtube">https://www.youtube.com/</span>
                    </div>
                    <input type="text" class="form-control" id="youtube" name="youtube_user"  value="{{ $general->youtube_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input youtube username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="google-plus" class="sr-only">Google Plus</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="google-plus">https://plus.google.com/</span>
                    </div>
                    <input type="text" class="form-control" id="google-plus" name="google_plus_user"  value="{{ $general->google_plus_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input google plus username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="instagram" class="sr-only">Instagram</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="instagram">https://instagram.com/</span>
                    </div>
                    <input type="text" class="form-control" id="instagram" name="instagram_user"  value="{{ $general->instagram_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input Instagram username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="linkedin" class="sr-only">Linked In</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="linkedin">https://linkedin.com/</span>
                    </div>
                    <input type="text" class="form-control" id="linkedin" name="linkedin_user"  value="{{ $general->linkedin_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input Linkedin username.
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="pinterest" class="sr-only">Pinterest</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="pinterest">https://pinterest.com/</span>
                    </div>
                    <input type="text" class="form-control" id="pinterest" name="pinterest_user"  value="{{ $general->pinterest_user }}"  placeholder="Username">
                    <div class="invalid-feedback">
                        Please input Pinterest username.
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-save"></i>
                    Save
                </button>
            </div>
        </form>
    </div>
@endsection

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $(document).ready(function () {
            $("#logo").on('change', function (event) {
                var placeholder = $("#logoLabel").data('placeholder')

                if (event.target.files.length) {
                    placeholder = '';

                    for (var index = 0; index < event.target.files.length; index++) {
                        if (index > 0) {
                            placeholder += ", ";
                        }

                        placeholder += event.target.files[index].name
                    }
                }

                $("#logoLabel").html(placeholder)
            })
        })
    </script>
@endpush
