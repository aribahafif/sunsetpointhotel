@extends('layouts.admin', [
'title' => 'Header Setting'
])

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ url('/config/header') }}"  enctype="multipart/form-data">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            <div class="d-flex align-items-center" style="margin-bottom: 8px;">
                <label class="control-label" style="margin: 0;">Titles</label>
                <div class="btn btn-danger btn-sm add-more-title ml-auto">
                    <i class="fa fa-plus"></i>
                    Add
                </div>
            </div>

            <div class="form-group title">
                @foreach($header->title as $title)
                    <div class="input-group copy" style="margin-bottom: 8px;">
                        <input type="text" class="form-control" id="title" name="title[]" placeholder="Please enter title..." value="{{ $title }}" required />
                        <div class="input-group-prepend">
                            <div type="button" class="btn btn-default remove-title-input"><i class="fa fa-minus"></i></div>
                        </div>
                    </div>
                @endforeach

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a title </div>
            </div>

            <div class="form-group">
                <label for="subtitle">Subtitle</label>
                <textarea name="subtitle" id="subtitle" class="form-control" placeholder="Please enter subtitle...">
                    {{ $header->subtitle }}
                </textarea>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a subtitle </div>
            </div>

            <label class="control-label" style="margin: 0;">Cover</label>
            <br />

            @if(!empty($header->cover))
                <img src="{{ url($header->cover) }}" class="img-fluid mx-auto pb-2">
            @endif

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="cover" name="cover" value="" accept="image/*">
                    <label class="custom-file-label" for="cover" id="coverLabel" data-placeholder="Choose header cover photo...">
                        Choose header cover photo...
                    </label>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-save"></i>
                    Save</button>
            </div>
        </form>
    </div>
@endsection

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $(document).ready(function() {
            $("body").on("click", ".add-more-title", function () {
                $(".form-group.title").append(
                    `<div class="input-group copy" style="margin-bottom: 8px;">
                <input type="text" class="form-control" id="title" name="title[]" placeholder="Please enter title..." value="" required />
                <div class="input-group-prepend">
                    <div type="button" class="btn btn-default remove-title-input"><i class="fa fa-minus"></i></div>
                </div>
            </div>`
                )
            });

            $("body").on("click", ".remove-title-input", function () {
                $(this).closest('.input-group.copy').remove();
            })

            $("#cover").on('change', function (event) {
                var placeholder = $("#coverLabel").data('placeholder')

                if (event.target.files.length) {
                    placeholder = '';

                    for (var index = 0; index < event.target.files.length; index++) {
                        if (index > 0) {
                            placeholder += ", ";
                        }

                        placeholder += event.target.files[index].name
                    }
                }

                $("#coverLabel").html(placeholder)
            })
        });
    </script>
@endpush