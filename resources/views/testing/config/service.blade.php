@extends('layouts.admin', [
'title' => 'Service Setting'
])

@section('content')
    <div class="card-body">
        <form method="POST" action="{{ url('/config/service') }}"  enctype="multipart/form-data">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            <div class="form-group title">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $service->title }}" placeholder="Please enter title..." required>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a title </div>
            </div>

            <div class="form-group">
                <label for="subtitle">Subtitle</label>
                <textarea name="subtitle" id="content" class="form-control" placeholder="Please enter subtitle...">
                    {!! $service->subtitle !!}
                </textarea>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a subtitle </div>
            </div>

            <div class="d-flex justify-content-center">
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#cover").on('change', function (event) {
                var placeholder = $("#coverLabel").data('placeholder')

                if (event.target.files.length) {
                    placeholder = '';

                    for (var index = 0; index < event.target.files.length; index++) {
                        if (index > 0) {
                            placeholder += ", ";
                        }

                        placeholder += event.target.files[index].name
                    }
                }

                $("#coverLabel").html(placeholder)
            })
        })
    </script>
@endpush