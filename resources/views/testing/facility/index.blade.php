@extends('layouts.admin', [
'title' => 'Facilities',
'url' => url('facilities/new')
])

@section('content')
    <div class="card-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($facilities as $index => $facility)
                <tr>
                    <td>{{ ++$index }}</td>
                    <td>{{ $facility->title }}</td>
                    <td>{{ $facility->description }}</td>
                    <td><img src="{{ url($facility->image) }}" height="30px" /></td>
                    <td class="btn-group" role="group">
                        <a href="{{ url('facilities/'.$facility->id.'/edit') }}" class="btn btn-sm btn-warning"  data-tooltip="tooltip"  title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="{{ url('facilities/'.$facility->id.'/edit') }}" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#published" data-tooltip="tooltip"  title="{{ $facility->status == 'published' ? 'unpublished' : 'published' }}">
                            <i class="fa fa-{{ $facility->status == 'published' ? 'check' : 'times' }}"></i>

                        </a>

                        <!-- Modal -->
                        <div class="modal fade" id="published" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Change status</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to {{ $facility->status == 'published' ? 'Unpublished' : 'Published' }} ?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" action="{{ url('facilities/'.$facility->id.'/edit') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="status" value="{{ $facility->status == 'published' ? 'unpublished' : 'published' }}">
                                            {{-- <input type="submit" value="{{ $val->status == 'published' ? 'Unpublished' : 'Published' }}"> --}}
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">{{ $facility->status == 'published' ? 'Unpublished' : 'Published' }}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="{{ url('facilities/'.$facility->id.'/delete') }}" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete" data-tooltip="tooltip"  title="Delete">
                            <i class="fa fa-trash"></i>
                        </a>

                        <!-- Modal -->
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete facility</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure delete {{ $facility->title }}?
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" id="delete" action="{{ url('facilities/'.$facility->id.'/delete') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('js/datatable.js') }}"></script>
@endpush