@extends('layouts.admin', [
'title' => isset($facility) ? 'Update facility ' .$facility->title : 'New facility'
])

@section('content')

    <div class="card-body">

        <form method="post" class="needs-validation" novalidate action="{{ isset($facility) ? url("facilities/{$facility->id}/edit") : url("facilities") }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" type="text" name="title" value="{{ isset($facility) ? $facility->title : '' }}" placeholder="Title" required>

                <div class="valid-feedback"> Looks good! </div>
                <div class="invalid-feedback"> Please input a title </div>
            </div>

            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <textarea class="form-control" id="content" name="description" cols="30" rows="10" required>{{ isset($facility) ? $facility->description : '' }}</textarea>

                <div class="valid-feedback"> Looks good! </div>

                <div class="invalid-feedback"> Please input a description </div>
            </div>

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image" name="image" {{ isset($facility) ?: 'required' }} accept="image/*" />
                    <label class="custom-file-label" for="image">Choose file</label>

                    <div class="valid-feedback">Looks good!</div>

                    <div class="invalid-feedback">Please upload a image</div>
                </div>
            </div>

            @if(isset($facility))
                <h3>old image</h3>
                <img class="img-fluid" src="{{ url($facility->image) }}"/>
            @endif

            <div class="form-group">
                <label for="status" class="control-label">Status</label>
                <select class="custom-select" name="status" value="{{ isset($facility) ? $facility->status : '' }}">
                    <option value="published">Published</option>
                    <option value="unpublished">Unpublished</option>
                </select>
            </div>

            <div class="pt-5 d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-save"></i>
                    {{ isset($facility) ? 'Update' : 'Save' }}
                </button>
            </div>

        </form>
    </div>
@endsection


@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('js/editor.js') }}"></script>
@endpush