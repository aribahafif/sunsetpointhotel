@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="fa fa-fast-forward text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Facilities</div>
                            <div class="stat-digit">{{ $facility }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="fa fa-home text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Rooms</div>
                            <div class="stat-digit">{{ $room }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="fa fa-user text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Testimony</div>
                            <div class="stat-digit">{{ $testimoni }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-one">
                        <div class="stat-icon dib"><i class="fa fa-users text-success border-success"></i></div>
                        <div class="stat-content dib">
                            <div class="stat-text">Users</div>
                            <div class="stat-digit">{{ $user }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection