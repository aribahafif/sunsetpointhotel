@if(isset($title))
<div class="row card-header">
    <div class="col">
        <h3 class="card-title">{{ $title or 'Dashboard' }}</h3>
    </div>

    @if(isset($url))
        <div class="col text-right">
            <a class="btn btn-sm btn-primary" href="{{ $url }}"><i class="fa fa-plus"></i> New</a>
        </div>
    @endif
    @if(isset($show))
        <div class="col text-right">
            <a class="btn btn-sm btn-warning" href="{{ $show }}"><i class="fa fa-eye"></i> Show</a>
        </div>
    @endif
</div>
@endif