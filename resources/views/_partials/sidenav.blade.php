<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/" style="font-size: 20px;">{{ config('app.name')." CMS" }}</a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/home') }}"> <i class="menu-icon fa fa-home"></i>Home </a>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-hotel"></i>Rooms</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-circle"></i><a href="{{ route('rooms.index') }}">Rooms</a></li>
                        <li><i class="fa fa-circle"></i><a href="{{ route('rooms.create') }}">Creating room</a></li>
                        <li><i class="fa fa-circle"></i><a href="{{ route('room-facility.index') }}">Room Facility</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-bath"></i>Services</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-circle"></i><a href="{{ url('facilities') }}">All services</a></li>
                        <li><i class="menu-icon fa fa-circle"></i><a href="{{ url('facilities/new') }}">create Facility</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-user-circle"></i>Testimonies</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-users"></i><a href="{{ url('testimonies') }}">All testimonies</a></li>
                        <li><i class="menu-icon fa fa-user-o"></i><a href="{{ url('testimonies/new')}}">create testimoni</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-cog"></i>Setting</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-check"></i><a href="{{ url('config/header') }}">Header</a></li>
                        <li><i class="menu-icon fa fa-check"></i><a href="{{ url('config/service') }}">Service</a></li>
                        <li><i class="menu-icon fa fa-check"></i><a href="{{ url('config/room') }}">Room</a></li>
                        <li><i class="menu-icon fa fa-check"></i><a href="{{ url('config/about') }}">About </a></li>
                        <li><i class="menu-icon fa fa-check"></i><a href="{{ url('config/general') }}">General</a></li>
                    </ul>
                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->