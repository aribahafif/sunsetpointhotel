@extends('layouts.app', [
'title' => 'Sunset Point Hotel'])

@section('content')

@include('home.header')
@include('home.facility')
@include('home.room')
@include('home.testimoni')
@include('home.about')
@endsection