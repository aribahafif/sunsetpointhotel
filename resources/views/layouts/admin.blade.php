<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ app()->getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>{{ (isset($title) && $title ? "$title - " : "").config('app.name')." | CMS" }}</title>
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @stack('styles')
    {{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>--}}
</head>
<body>
@include('_partials.sidenav')
<!-- Right Panel -->

<div id="right-panel" class="right-panel">
    @include('_partials.header')
    @include('_partials.breadcrumb')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    @include('_partials.card-header')
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div><!-- /#right-panel -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/manifest.js')}}"></script>
<script src="{{ asset('js/vendor.js')}}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
@stack('scripts')
@if (Session::has('status'))
    <script>
        $(document).ready(function() {
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('{{ Session::get('status') }}');

            }, 1300);
        });

    </script>
@endif
@if (count($errors) > 0)
    <script>
        @foreach ($errors->all() as $error)
        $(document).ready(function() {
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('{{ $error }}');

            }, 1300);
        });

        @endforeach
    </script>
@endif
@if(config('app.env') == 'local')
    <script src="http://localhost:35729/livereload.js"></script>
@endif
</body>
</html>
