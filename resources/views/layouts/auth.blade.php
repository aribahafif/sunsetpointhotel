<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>{{ (isset($title) && $title ? "$title - " : "").config('app.name')." | CMS" }}</title>
  <link href="{{ asset('css/auth.css') }}" rel="stylesheet">
</head>

<body class="text-center">
@yield('content')
<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/auth.js') }}"></script>
</body>
</html>
