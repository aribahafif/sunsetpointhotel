<div id="testimoni" class="pt-5">
    <h3><i class="fa fa-quote-left fa-2x"></i></h3>
    <div id="testimoniUsers" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($testimonies as $testi)
                <li data-target="#testimoniUsers" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($testimonies as $testi)
                <div class="carousel-item {{ $loop->first ? ' active' : '' }}">
                    <div class="carousel-caption">
                        <p>{{ $testi->content }}</p>
                        <h5 class="mt-5">{{ $testi->name }}</h5>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>