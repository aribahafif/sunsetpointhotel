<section id="room" class="room cards-room">
    <div class="container">
        <div class="text-center heading">
            <h3 class="title">{{ $config->room->title }}</h3>
            <p class="subtitle">{{ $config->room->subtitle }}</p>
        </div>
        <div class="row">
            @foreach($rooms as $index => $room)
                <div class="col-md-6 col-lg-4  wow bounceInUp animation-delay-{{ ++$index * 2 }}">
                    <div class="card border-0 transform-on-hover">
                        <a href="{{ route('our-room', $room->slug) }}">
                            <img src="{{ asset($room->images->first()->url) }}" alt="Card Image" class="card-img-top">
                        </a>
                        <div class="card-body">
                            <h6><a href="{{ route('our-room', $room->slug) }}">{{ $room->name }}</a></h6>
                            <p class="text-muted card-text">start {{ $room->harga }} /night</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>