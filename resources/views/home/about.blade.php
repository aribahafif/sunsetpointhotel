<section id="about" class="about pt-5 mt-5">
	<div class="container">
		<div class="row">
			<div id="about-hotel" class="col-md-4">
				<h3 id="about" class="title wow fadeInLeft  animation-delay-5">{{ $config->about->title }}</h3>
				<div class="wow fadeInLeft  animation-delay-8">{!! $config->about->content !!}</div>
			</div>

			<div class="col-md-8">
				<div class="">
					@foreach($media as $index => $eachMedia)
					<a href="{{ url($eachMedia->url) }}" data-lightbox="image-1" data-title="My gallery {{ $eachMedia->id }}" class="wow fadeInLeft  animation-delay-{{ $index+1 }}">
						<img height="150" src="{{ url($eachMedia->url) }}" />
					</a>
					@endforeach
				</div> 
				
			</div>

		</div>
	</div>
</section>
