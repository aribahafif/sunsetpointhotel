<section id="facility" class="text-center">
    {{-- <div class="layer"></div> --}}
    <div id="service" class="pt-5 ">
        <h2 class="title wow slideInLeft animation-delay-6">{{ $config->service->title }}</h2>
        <p class="pr-5 pl-5 mr-5 ml-5 wow slideInLeft animation-delay-10">{!! $config->service->subtitle !!}</p>
    </div>

    <div class="container">
        {{-- <img src="{{ $config->service->cover }}" class="img-fluid wow bounceInUp animation-delay-12" alt="facility"> --}}
        <div class="row p-5">
            @foreach($facilities  as $index => $facility)
                <div class="col-md-4 wow fadeInDown  animation-delay-{{ ++$index * 2 }}">
                    <img src="{{ url($facility->image) }}">
                    <h3>{{ $facility->title }}</h3>
                    <p>{{ $facility->description }}</p>
                </div>
            @endforeach
        </div>
    </div>
</section>