<section id="check" class="check pb-5 pt-5">
    <div class="d-flex justify-content-center">
        <h3 class="title">Check Available</h3>
    </div>

    <div class="container">

        <p class="text-center pr-5 pl-5 ml-5 mr-5">Simply fill required fields and then click on check avability button then you will be redirect to avaiable room and suites to book online</p>

        <form action="" class="needs-validation" novalidate>
            <div class="row">
                <div class="form-group col">
                    <label class="sr-only" for="departure">Departure date</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="departure" placeholder="Departure Date" required>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="date departure"><i class="fa fa-calendar"></i> </span>
                        </div>
                        <div class="invalid-feedback">
                            Please choose a date to checkin.
                        </div>
                    </div>
                </div>
                <div class="form-group col">
                    <label for="arrival" class="sr-only">Arrival Date</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="arrival" placeholder="Arrival Date" required>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="date arrival"><i class="fa fa-calendar"></i> </span>
                        </div>
                        <div class="invalid-feedback">
                            Please choose a date to checkout.
                        </div>
                    </div>
                </div>

                <div class="form-group col">
                    <label for="guest" class="sr-only">Guest</label>
                    <input type="number" class="form-control" id="guest" placeholder="Guest" required>
                    <div class="invalid-feedback">
                        Please choose a number of person.
                    </div>
                </div>

                <div class="form-group col">
                    <label for="room" class="sr-only">Room type</label>
                    <input type="text" class="form-control" id="room" placeholder="Room type" required>
                    <div class="invalid-feedback">
                        Please choose a room type.
                    </div>
                </div>
            </div>

            <div class="mt-5 text-center">
                <button type="submit" class="btn btn-check mb-2">Check Available</button>
            </div>

        </form>

    </div>
</section>