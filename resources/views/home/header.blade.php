<header id="masthead" style="background-image: url({{ $config->header->cover }})">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="intro-text">
				<div id="typed-strings">
					@foreach($config->header->title as $title)
						<p>{!! $title !!}</p>
					@endforeach
				</div>
				<h1 class="intro-lead-in"><span id="typed"></span></h1>
				<div class="subtitle">
					<p>{!! $config->header->subtitle !!}</p>
					<a href="#facility" class="btn btn-check js-scroll-trigger">Get Started</a>
				</div>
			</div>
		</div>
	</div>
</header>
@push('scripts')
	<script src="{{ asset('js/typed.js') }}"></script>
@endpush