<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <h4>Book Now</h4>
                <p class="">We are ready to sale</p>
                <p>{{ $config->general->email }}</p>
            </div>
            <div class="col-lg-3 col-md-6">
                <h4>Location</h4>
                <address>
                    {!! $config->general->location !!}
                </address>
            </div>

            <div class="col-lg-2 col-md-6">
                <h4>Contact</h4>
                <address>
                    <p class="">{{ $config->general->email }}</p>
                    {!! $config->general->phone !!}
                </address>
            </div>

            <div class="col-lg-2 col-md-6">
                <img class="logo" src="{{ $config->general->logo }}">
            </div>

            <div class="col-lg-2 text-center">
                <div class="social">
                    @if(!empty($config->general->facebook_user))
                        <a href="https://www.facebook.com/{{ $config->general->facebook_user }}" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
                    @endif
                    @if(!empty($config->general->twitter_user))
                        <a href="https://www.twitter.com/{{ $config->general->twitter_user }}" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
                    @endif
                    @if(!empty($config->general->instagram_user))
                        <a href="https://www.instagram.com/{{ $config->general->instagram_user }}" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
                    @endif
                    @if(!empty($config->general->google_plus_user))
                        <a href="https://plus.google.com/{{ $config->general->google_plus_user }}" target="_blank" class="instagram"><i class="fa fa-google-plus"></i></a>
                    @endif
                    @if(!empty($config->general->youtube_user))
                        <a href="https://www.youtube.com/{{ $config->general->youtube_user }}" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a>
                    @endif
                    @if(!empty($config->general->linkedin_user))
                        <a href="https://www.linkedin.com/{{ $config->general->linkedin_user }}" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    @endif
                    @if(!empty($config->general->pinterest_user))
                        <a href="https://www.pinterest.com/{{ $config->general->pinterest_user }}" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a>
                    @endif

                </div>
            </div>
        </div>
    </div>


</footer>
{{--<div class="p-3 bg-black">--}}
{{--<p class="footer text-white">&copy; Copyright {{ config('app.name') }} {{ date('Y') }} </p>--}}
{{--</div>--}}
