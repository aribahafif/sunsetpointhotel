
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <a class="navbar-brand js-scroll-trigger" href="{{ url('/#masthead') }}">
    <img width="30" height="30" class="d-inline-block align-top" src="{{ $config->general->logo }}">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav text-uppercase ml-auto">
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="{{ url('/#service') }}">Service</a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="{{ url('/#room') }}">Room</a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="{{ url('/#testimoni') }}">Testimony</a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger" href="{{ url('/#gallery') }}">Gallery</a>
      </li>
    </ul>
  </div>
</nav>