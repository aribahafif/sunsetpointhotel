@extends('layouts.app')

@section('content')
<section id="room-content">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <form action="">
                <div class="form-group">
                    <label for="exampleInputEmail1">Contact Name</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="contact name">
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="phone">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="email">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="exampleInputEmail1">Guest Name</label>
                    <input type="email" class="form-control col-md-6" id="exampleInputEmail1" placeholder="Guest Name">
                </div>
            </form>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Additional Request</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>

            <div class="row custom-row-book-room">
                <p>$ 100.00;</p>
                <a href={{ url("/")}} class="btn btn-orange">Book Now ! <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
</div>
<br><br><br>
</section>
@endsection 