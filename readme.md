# README FIRST

## Apa yang di butuhkan

- [composer](https://getcomposer.org/download/), 
- [nodejs](https://nodejs.org/en/), 
- [php7.1](http://php.net/downloads.php)
- Ekstensi PHP OpenSSl
- Ekstensi PHP  PDO
- Ekstensi PHP Mbstring
- Ekstensi PHP Tokenizer
- Ekstensi PHP XML
- Ekstensi PHP Ctype
- Ekstensi PHP JSON

Jika menggunakan docker maka sebaikanya menggunakan laradock dengan mengaktifkan nodejs, image_optimizer dan imagemagick.

## Instalasi

1. ketik `git clone git@gitlab.com:oriontechno/sunsetpoint-compro.git`
2. masuk ke direktori project `cd sunsetpoint-compro`
3. install depedency php dengan composer `composer install`
4. ketik `cp .env.example .env`
5. ketik `php artisan key:generate` untuk menggenerate toke laravel
6. install depedendecy javascript dengan mengetikan `npm i` setelelah selesai kemudian `npm run prod`

## Native PHP Server

Cukup dengan menjalankan `php artisan serve` maka 

## Konfigurasi applikasi

1. Masuk ke direcetory project `cd ~/Code/<project folder>` dan lakukan  `composer install` kemudian `npm i`
2. Copy .env.example `cp .env.example .env`  dan resource/compro.json.example `cp resource/compro.json`. Kemudian jalankan perintah `php artisan key:generate`
3. JIka menggunakan database mysql, maka default konfigurasi database yang digunakan seperti berikut ini:

    ```
    DB_CONNECTION=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_DATABASE=nama_database
    DB_USERNAME=nama_username
    DB_PASSWORD=password
    ```

4. Download file asset gambar di [google drive](https://drive.google.com/file/d/1MCydb_ARfMXtJstiKOhClsTIrlomc8IU/view?usp=sharing) dan simpan di direktrori project `storage/app/public` sehingga struktur directorinya  terlihat seperti berikut ini:
    ```
    ├── facilities
    │   ├── 5-stars.svg
    │   ├── comment-black-oval-bubble-shape.svg
    │   ├── facebook-placeholder-for-locate-places-on-maps.svg
    │   ├── key-hole.svg
    │   ├── magnifying-glass.svg
    │   ├── mail.svg
    │   ├── pencil-edit-button.svg
    │   ├── phone.svg
    │   ├── play-button.svg
    │   ├── right-arrow.svg
    │   ├── swimming-pool.svg
    │   └── washing-machine.svg
    ├── facility.jpg
    ├── images
    │   ├── 10-image.jpg
    │   ├── 11-image.jpg
    │   ├── 12-image.jpg
    │   ├── 13-image.jpg
    │   ├── 14-image.jpg
    │   ├── 1-image.jpg
    │   ├── 2-image.jpg
    │   ├── 3-image.jpg
    │   ├── 4-image.jpg
    │   ├── 5-image.jpg
    │   ├── 6-image.jpg
    │   ├── 7-image.jpg
    │   ├── 8-image.jpg
    │   ├── 9-image.jpg
    │   └── admin.jpg
    ├── logo.svg
    └── rooms
        ├── 10-room.jpg
        ├── 11-room.jpg
        ├── 12-room.jpg
        ├── 13-room.jpg
        ├── 14-room.jpg
        ├── 1-room.jpg
        ├── 3-room.jpg
        ├── 4-room.jpg
        ├── 5-room.jpg
        ├── 6-room.jpg
        ├── 7-room.jpg
        ├── 8-room.jpg
        └── 9-room.jpg

    ```
5. Jalankan migrasi dan seed database dengan perintah `php artisan migrate --seed` dan `npm run prod`
6. Symlink folder `storage` ke public dengan menjalankan perintah `php artisan storage:link`. **Hal ini penting untuk menampilkan file gambar yang di upload**
7. JIka Anda menjalankan aplikasi dengan perintah `php artisan serve`, pastikan dulu bahwa aplikasi yang terinstall terhubung dengan database


## Login

- email `admin@gmail.com`
- password `secret`

## Troubleshooting

Jika waktu menjalankan `npm run prod` pada *workspace* laradock gagal maka bisa dengan menginstall/memasang **nodejs** pada komputer host dengan versi nodejs yang sama pada laradock.

