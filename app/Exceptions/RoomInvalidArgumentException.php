<?php

namespace App\Exceptions;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class RoomInvalidArgumentException extends InvalidArgumentException
{
    //
}
