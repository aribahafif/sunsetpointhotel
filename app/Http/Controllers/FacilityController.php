<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;


class FacilityController extends Controller
{

    /**
     * FacilityController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $facilities = Facility::all();

        return view('testing.facility.index', [
            'facilities'  => $facilities
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'description'   => 'required',
            'image'         => 'required|image'
        ]);

        if ($request->get('status')) {
            $rules['status'] = [Rule::in(['published', 'unpublished'])];
        }

        $data = $request->only('title', 'description');
        $data['image'] = basename($request->file('image')->store('facilities'));
        $facility = Facility::firstOrCreate([
            'title'         => $data['title'],
            'description'   => $data['description']
        ], [
            'image' => $data['image']
        ]);

        return redirect('/facilities')
                ->with('status', 'success')
                ->with('message', "Facility id: {$facility->id}");
    }

    /**
     * @param $facilityId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($facilityId)
    {
        $facility = Facility::find($facilityId);

        if (!$facility) {
            return redirect('/facilities')
                ->with('status', 'error')
                ->with('message', "Facility not found.");
        }

        return view('testing.facility.form', [
            'facility' => $facility
        ]);
    }

    /**
     * @param Request $request
     * @param $facilityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $facilityId)
    {
        $facility = Facility::find($facilityId);

        if (!$facility) {
            return redirect('/facilities')
                ->with('status', 'error')
                ->with('message', "Facility not found.");
        }
        if ($request->get('status')) {
            $rules['status'] = [Rule::in(['published', 'unpublished'])];
        }

        $this->validate($request, [
            'title'         => 'sometimes|required',
            'description'   => 'sometimes|required',
            'image'         => 'sometimes|image',
            'status'        => [
                'sometimes',
                Rule::in(['published', 'unpublished'])
            ]
        ]);

        $data = $request->only('title', 'description');

        if (array_key_exists('status', $data) && $data['status'] == 'published') {
            if ((Facility::whereStatus('published')->get())->count() >= 6) {
                return redirect()
                    ->back()
                    ->with('status', 'error')
                    ->message('message', 'Jumlah fasilitas yang dapat ditampilkan melibihi maksimal (6). Silahkan unpublished salah satu terlebih dahulu.');
            }
        }

        if ($request->file('image')) {
            $data['image'] = basename($request->file('image')->store('facilities'));
            Storage::delete('facilities/'.basename($facility->image));
        }

        foreach ($data as $key => $value) {
            $facility->{$key} = $value;
        }

        $facility->save();

        return redirect('/facilities')
                ->with('status', 'success')
                ->with('message', "Facility id: {$facility->id} updated.");

    }

    /**
     * @param $facilityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($facilityId)
    {
        $facility = Facility::find($facilityId);

        if (!$facility) {
            return redirect('/facilities')
                ->with('status', 'error')
                ->with('message', "Facility not found.");
        }

        if ($facility->delete()) {
            Storage::delete('facilities/'.basename($facility->image));
        } else {
            return redirect('/facilities')
                ->with('status', 'error')
                ->with('message', "Facility id: {$facility->id} delete failed.");
        }

        return redirect('/facilities')
                ->with('status', 'success')
                ->with('message', "Facility id: {$facility->id} deleted.");
    }
}
