<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\RoomRequest;
use App\Models\RoomFacility;
use App\Models\RoomImage;
use Carbon\Carbon;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class RoomController extends Controller
{
    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {   

        $rooms = Room::with('room_facilities','images')->get();

       // dd($rooms);

        return view('testing.room.index', [
            'rooms' => $rooms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allFacilities = RoomFacility::all();
        return view('testing.room.form', compact('allFacilities'));
    }

    /**
     * @param RoomRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoomRequest $request)
    {
        $room = Room::create($request->all());

        if ($request->images) {
            foreach ($request->images as $image){
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 1280, 720 )->save(storage_path(). '/app/public/rooms/' . $filename );
                Image::make( $image )->fit( 500, 480)->save(storage_path(). '/app/public/rooms/thumbs/' . $filename );
                $image                = new RoomImage();
                $image->src         = $filename;
                $image->room_id = $room->id;
                $image->save();
            }
        }


        if (isset($request->facilities)){
            $room->room_facilities()->sync($request->facilities);
        }else {
            $room->room_facilities()->sync(array());
        }

        return redirect('/rooms')
        ->with('status', 'success')
        ->with('message', "Rooms id: {$room->id}");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::with('room_facilities')->find($id);

        if (!$room) {
            return redirect('/rooms')
                ->with('status', 'error')
                ->with('message', "Room not found.");
        }

        $allFacilities = RoomFacility::select('name', 'id')->get();
        $facilities = $room->room_facilities->map(function ($facility) {
            return $facility->id;
        });

        return view('testing.room.form', [
            'room' => $room,
            'allFacilities' => $allFacilities,
            'facilities'    => $facilities->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $room = Room::find($id);

        if (!$room) {
            return redirect('/rooms')
                ->with('status', 'error')
                ->with('message', "Room not found.");
        }

        if ($request->get('status')) {
            $rules['status'] = [Rule::in(['published', 'unpublished'])];
        }

        $this->validate($request, [
            'name'         => 'sometimes|required',
            'image'         => 'sometimes|image',
            'status'        => [
                'sometimes',
                Rule::in(['published', 'unpublished'])
            ]
        ]);

        $data = $request->only('name', 'detail', 'price', 'status');
        
        if ($request->images) {
            foreach ($request->images as $image){
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 1280, 720 )->save(storage_path(). '/app/public/rooms/' . $filename );
                Image::make( $image )->fit( 500, 480)->save(storage_path(). '/app/public/rooms/thumbs/' . $filename );
                $image                = new RoomImage();
                $image->src         = $filename;
                $image->room_id = $room->id;
                $image->save();
            }
        }

        if (isset($request->facilities)){
            $room->room_facilities()->sync($request->facilities);
        } else {
            $room->room_facilities()->sync(array());
        }


        foreach ($data as $key => $value) {
            $room->{$key} = $value;
        }

        $room->save();

        return redirect('/rooms')
                ->with('status', 'success')
                ->with('message', "Facility id: {$room->id} updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);

        if (!$room) {
            return redirect('/rooms')
                ->with('status', 'error')
                ->with('message', "Room not found.");
        }

        if ($room->delete()) {
            Storage::delete('rooms/'.basename($room->image));
        } else {
            return redirect('/rooms')
                ->with('status', 'error')
                ->with('message', "Room id: {$room->id} delete failed.");
        }

        return redirect('/rooms')
                ->with('status', 'success')
                ->with('message', "Room id: {$room->id} deleted.");
    }


    public function formRemoveImage($id){
        $image = RoomImage::findOrFail($id);
        // dd($image);
        return view('testing.room.delete', compact('image'));
    }

    public function removeImage($id)
    {
        $image = RoomImage::find($id);

        if (!$image) {
            return redirect()->back()
                ->with('status', 'error')
                ->with('message', 'Media not found.');
        }

        if (!$image->delete()) {
            return redirect()->back()
                ->with('status', 'error')
                ->with('message', 'Media delete failed.');
        }

        Storage::delete("rooms/{$image->src}");
        
        // return response()->json();

        return redirect()->back()
            ->with('status', 'success')
            ->with('message', 'Media deleted.');
    }

    /**
     * process image to default resolution HD 1024 x 768 for all image upload
     * the image is a resolution height 768px
     *
     * @param $image
     * @return null|string
     */
    private function processImage($image)
    {
        $imageName = str_slug($image->getClientOriginalName(), '-').'-'.(Carbon::now())->toAtomString().'.jpg';
//        $imageName = $image;
        $processedImage = Image::make($image[]);
        $processedImage->resize(null, 768, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $startingX = floor(($processedImage->width() / 2) - 512);
        $startingY = floor(($processedImage->height() / 2) - 384);
        $processedImage->crop(1024, 768, $startingX, $startingY);

        if (!Storage::put("rooms/{$imageName}", (string) $processedImage->encode('jpg', 30))) {
            $imageName = null;
        }

        return $imageName;
    }
}
