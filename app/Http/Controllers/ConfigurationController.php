<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Media;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ConfigurationController extends Controller
{
    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editHeaderSection()
    {
        $config = json_decode(file_get_contents(resource_path('compro.json')));
        $headerConfig = $config->header;
        $headerConfig->cover = Storage::url("configs/{$headerConfig->cover}");

        return view('testing/config/header', [
          'header' => $headerConfig
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editServiceSection()
    {
        $config = json_decode(file_get_contents(resource_path('compro.json')));
        $serviceConfig = $config->service;

        return view('testing/config/service', [
          'service' => $serviceConfig
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editRoomSection()
    {
        $config = json_decode(file_get_contents(resource_path('compro.json')));
        $roomConfig = $config->room;

        return view('testing/config/room', [
          'room' => $roomConfig
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGeneralSection()
    {
        $config = json_decode(file_get_contents(resource_path('compro.json')));
        $generalConfig = $config->general;
        $generalConfig->logo = Storage::url("configs/{$generalConfig->logo}");

        return view('testing/config/general', [
          'general' => $generalConfig
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editAboutSection()
    {
        $config = json_decode(file_get_contents(resource_path('compro.json')));
        $aboutConfig = $config->about;
        $media = Media::where('mediaable_id', null)->where('mediaable_type', null)->get();

        return view('testing/config/about/index', [
          'about' => $aboutConfig,
          'media' => $media
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateHeaderSection(Request $request)
    {
        $rules = [
            'title'     => 'array|min:1',
            'title.*'   => 'required|string',
            'subtitle'  => 'sometimes|string',
        ];

        if ($request->file('cover')) {
            $rules['cover'] = 'required|image|max:2048';
        }

        $this->validate($request, $rules);
        
        $data = $request->only('title', 'subtitle');
        $config = json_decode(file_get_contents(resource_path('compro.json')));

        if ($request->file('cover')) {
            $newCoverName = basename($request->file('cover')->store('configs'));
            Storage::delete("configs/{$config->header->cover}");
            $data['cover'] = $newCoverName;
        }

        foreach ($data as $index => $value) {
            $config->header->{$index} = $value;
        }

        file_put_contents(resource_path('compro.json'), json_encode($config));

        return redirect('/config/header')
                    ->with('status', 'success')
                    ->with('message', "Header setting updated");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateServiceSection(Request $request)
    {
        $rules = [
            'title'     => 'required|string',
            'subtitle'  => 'sometimes|string',
        ];

        $this->validate($request, $rules);
        
        $data = $request->only('title', 'subtitle');
        $config = json_decode(file_get_contents(resource_path('compro.json')));

        foreach ($data as $index => $value) {
            $config->service->{$index} = $value;
        }

        file_put_contents(resource_path('compro.json'), json_encode($config));

        return redirect('/config/service')
                    ->with('status', 'success')
                    ->with('message', "Service setting updated");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateRoomSection(Request $request)
    {
        $rules = [
            'title'     => 'required|string',
            'subtitle'  => 'sometimes|string',
        ];

        $this->validate($request, $rules);
        
        $data = $request->only('title', 'subtitle');
        $config = json_decode(file_get_contents(resource_path('compro.json')));

        foreach ($data as $index => $value) {
            $config->room->{$index} = $value;
        }

        file_put_contents(resource_path('compro.json'), json_encode($config));

        return redirect('/config/room')
                    ->with('status', 'success')
                    ->with('message', "Room setting updated");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGeneralSection(Request $request)
    {
        $rules = [
            'phone'  => 'required|string',
            'email'  => 'required|string|email',
            'location'  => 'required|string',
            'facebook_user' => 'present',
            'twitter_user'  => 'present',
            'youtube_user'  => 'present',
            'google_plus_user' => 'present',
            'instagram_user'   => 'present',
            'linkedin_user'   => 'present',
            'pinterest_user'   => 'present'
        ];

        if ($request->file('logo')) {
            $rules['logo'] = 'required|image|max:2048';
        }

        $this->validate($request, $rules);
        
        $data = $request->only('phone', 'email', 'location', 'facebook_user', 'twitter_user', 'youtube_user', 'google_plus_user', 'instagram_user', 'linkedin_user', 'pinterest_user');
        $config = json_decode(file_get_contents(resource_path('compro.json')));

        if ($request->file('logo')) {
            $newLogoName = basename($request->file('logo')->store('configs'));
            Storage::delete("configs/{$config->general->logo}");
            $data['logo'] = $newLogoName;
        }

        foreach ($data as $index => $value) {
            $config->general->{$index} = $value;
        }

        file_put_contents(resource_path('compro.json'), json_encode($config));

        return redirect('/config/general')
                    ->with('status', 'success')
                    ->with('message', "General setting updated");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAboutSection(Request $request)
    {
        $rules = [
            'title'    => 'required|string',
            'content'  => 'sometimes|string',
        ];

        $this->validate($request, $rules);
        
        $data = $request->only('title', 'content');
        $config = json_decode(file_get_contents(resource_path('compro.json')));

        foreach ($data as $index => $value) {
            $config->about->{$index} = $value;
        }

        file_put_contents(resource_path('compro.json'), json_encode($config));

        return redirect('/config/about')
                    ->with('status', 'success')
                    ->with('message', "About setting updated");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addMedia(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|max:2048'
        ]);

        if (!($mediaName = $request->file('image')->store('media'))) {
            return redirect('/config/about')
                        ->with('status', 'error')
                        ->with('message', 'File upload failed.');
        }

        Media::create([
            'name'  => basename($mediaName)
        ]);

        return redirect('/config/about')
                        ->with('status', 'success')
                        ->with('message', 'Media added.');
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param $mediaId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyMedia($mediaId)
    {
        $media = Media::find($mediaId);

        if (!$media) {
            return redirect('/config/about')
                        ->with('status', 'error')
                        ->with('message', 'Media not found.');
        }

        if (!$media->delete()) {
            return redirect('/config/about')
                        ->with('status', 'error')
                        ->with('message', 'Media delete failed.');
        }

        Storage::delete("media/{$media->name}");

        return redirect('/config/about')
                        ->with('status', 'success')
                        ->with('message', 'Media deleted.');
    }
}
