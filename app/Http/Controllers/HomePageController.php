<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Media;
use App\Models\Facility;
use App\Models\Testimony;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomePageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function index() {
        $config = json_decode(file_get_contents(resource_path('compro.json'), true));
        $config->header->cover = Storage::url("configs/{$config->header->cover}");
        $config->general->logo = Storage::url("configs/{$config->general->logo}");
        $media = Media::inRandomOrder()->where('mediaable_id', null)->where('mediaable_type', null)->take(6)->get();

        $facilities = Facility::whereStatus('published')->latest()->take(6)->get();
        $rooms = Room::whereStatus('published')->latest()->take(6)->get();
        $testimoni = Testimony::whereStatus('published')->latest()->get();

        return view('homepage', [
            'title'       => config('app.name'),
            'rooms'       => $rooms,
            'facilities'  => $facilities,
            'testimonies' => $testimoni,
            'config'      => $config,
            'media'       => $media
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function room($slug)
    {
        $config = json_decode(file_get_contents(resource_path('compro.json'), true));
        $config->header->cover = Storage::url("configs/{$config->header->cover}");
        $config->general->logo = Storage::url("configs/{$config->general->logo}");

        $queries = Room::with('room_facilities', 'images')->get();
        $latests = $queries->where('status', 'published')
            ->take(5);

        $room = $queries->where('slug', $slug)
            ->first();

       // dd($room->media);


        return view('roompage', [
            'config'      => $config,
            'latests' => $latests,
            'room' => $room
        ]);
    }

    public function bookRoom(){
                $config = json_decode(file_get_contents(resource_path('compro.json'), true));
        $config->header->cover = Storage::url("configs/{$config->header->cover}");
        $config->general->logo = Storage::url("configs/{$config->general->logo}");

        $queries = Room::with('room_facilities', 'images')->get();
        $latests = $queries->where('status', 'published')
            ->take(5);

        // $room = $queries->where('slug', $slug)
        //     ->first();

       // dd($room->media);


        return view('room_book', [
            'config'      => $config,
            'latests' => $latests,
            // 'room' => $room
        ]);
    }
}
