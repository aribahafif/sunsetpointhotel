<?php

namespace App\Http\Controllers;

use App\Models\Testimony;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TestimonyController extends Controller
{

    /**
     * TestimonyController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $testimonies = Testimony::all();

        return view('testing.testimony.index', [
            'testimonies'   => $testimonies
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'content'   => 'required',
            'name'      => 'required',
        ];

        if ($request->get('status')) {
            $rules['status'] = [Rule::in(['published', 'unpublished'])];
        }

        $this->validate($request, $rules);

        $data = $request->only('content', 'name', 'status');

        $testimony = Testimony::firstOrCreate([
            'content'   => $data['content'],
            'name'      => $data['name']
        ], [
            'status' => array_key_exists('status', $data) ? $data['status'] : 'published'
        ]);

        return redirect('/testimonies')
                    ->with('status', 'success')
                    ->with('message', "Testimony id: {$testimony->id}.");
    }

    /**
     * @param $testimonyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($testimonyId)
    {
        $testimony = Testimony::find($testimonyId);

        if (!$testimony) {
            return redirect('/testimonies')
                    ->with('status', 'error')
                    ->with('message', 'Testimony not found.');
        }

        return view('testing.testimony.form', [
            'testimony' => $testimony
        ]);
    }

    /**
     * @param Request $request
     * @param $testimonyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $testimonyId)
    {
        if ($request->get('content')) {
            $rules['content'] = 'required';
        }

        if ($request->get('name')) {
            $rules['name'] = 'required';
        }

        if ($request->get('status')) {
            $rules['status'] = [Rule::in(['published', 'unpublished'])];
        }

        $this->validate($request, $rules);

        $data = $request->only('content', 'name', 'status');

        $testimony = Testimony::find($testimonyId);

        if (!$testimony) {
            return redirect('/testimonies')
                    ->with('status', 'error')
                    ->with('message', 'Testimony not found.');
        }

        foreach ($data as $key => $value) {
            $testimony->{$key} = $value;
        }

        $testimony->save();

        return redirect('/testimonies')
                    ->with('status', 'success')
                    ->with('message', "Testimony id: {$testimony->id} updated.");
    }

    /**
     * @param $testimonyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($testimonyId)
    {
        $testimony = Testimony::find($testimonyId);

        if (!$testimony) {
            return redirect('/testimonies')
                    ->with('status', 'error')
                    ->with('message', 'Testimony not found.');
        }

        $testimony->delete();

        return redirect('/testimonies')
                    ->with('status', 'success')
                    ->with('message', "Testimony id: {$testimony->id} deleted.");
    }
}
