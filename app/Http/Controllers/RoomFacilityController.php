<?php

namespace App\Http\Controllers;

use App\Models\RoomFacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RoomFacilityController extends Controller
{
    /**
     * RoomFacilityController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = RoomFacility::all();
        return view('testing.room.facility.index', compact('facilities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('testing.room.facility.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->only('name');
        $data['image'] = basename($request->file('image')->store('room_facilities'));
        $facility = RoomFacility::firstOrCreate([
            'name'         => $data['name'],
        ], [
            'image' => $data['image']
        ]);

        return redirect()
            ->route('room-facility.index')
            ->with('success', 'Success')
            ->with('message', "Facility id: {$facility->id}");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = RoomFacility::findOrFail($id);
        return view('testing.room.facility.form', compact('facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $facility = RoomFacility::find($id);

        $this->validate($request, [
           'name' => 'sometimes:required',
           'image' => 'sometimes:image'
        ]);

        $data = $request->only('name');

        if ($request->file('image')) {
            $data['image'] = basename($request->file('image')->store('room_facilities'));
            Storage::delete('room_facilities/'.basename($facility->image));
        }

        foreach ($data as $key => $value) {
            $facility->{$key} = $value;
        }

        $facility->save();

        return redirect('room-facility.index')
            ->with('status', 'success')
            ->with('message', "Facility id: {$facility->id} updated.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility = RoomFacility::find($id);

        if (!$facility) {
            return redirect()
                ->route('room-facility.index')
                ->with('status', 'error')
                ->with('message', "Facility not found.");
        }

        if ($facility->delete()) {
            Storage::delete('room_facilities/'.basename($facility->image));
        } else {
            return redirect()
                ->route('room-facility.index')
                ->with('status', 'error')
                ->with('message', "Facility id: {$facility->id} delete failed.");
        }

        return redirect()
            ->route('room-facility.index')
            ->with('status', 'success')
            ->with('message', "Facility id: {$facility->id} deleted.");

    }
}
