<?php

namespace App\Models;

use App\Exceptions\RoomInvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use Parsedown;

class Room extends Model
{
    /**
     * @var string
     */
    protected $table='rooms';

    /**
     * @var array
     */
    protected $fillable=[
    	'name',
        'price',
        'detail'
    ];

    /**
     * set title attribute and automatically slug
     *
     * @param string $value
     *
     * */

        public function setNameAttribute($value)
        {
            $this->attributes['name'] = $value;
            if (! $this->exists){
                $this->setUniqueSlug($value, '');
            }
        }
    /**
     * recursive routine to set unique slug automatically
     *
     * @param string $title
     * @param mixed $extra
     *
     * */

    protected function setUniqueSlug($title, $extra)
    {
        $uuid = Uuid::uuid1();
        $slug = Str::slug($title.'-'.$extra);
        if (static::whereSlug($slug)->exists()){
            // upgrade php 7.1 can't use number
            $this->setUniqueSlug($title,$extra .$uuid->toString());
            return;
        }
        $this->attributes['slug'] = $slug;

    }

    /**
     * get excerpt of detail room
     *
     * @return String
     */
    public function getExcerptAttribute(): String
    {
        $markdown = new Parsedown();

        $content =  Str::limit($this->detail);
        return $markdown->text($content);
    }

    /**
     * get price room
     *
     * @return string
     */
    public function getHargaAttribute(){
        return "Rp " . number_format($this->price,2,',','.');
    }

    /**
     * get image url attribute mutator
     *
     * @param $value
     * @return mixed
     */
    public function getImageAttribute($value)
    {
        return Storage::url("rooms/{$value}");
    }

    /**
     * @return mixed
     */
    public function getCoverAttribute(){
        return Storage::url("{$this->images()->first()->src}");
    }

    /**
     * relationship between room and room facility many to many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function room_facilities(){
        return $this->belongsToMany(RoomFacility::class, 'room_facility_pivot');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(){
    	return $this->hasMany(RoomImage::class, 'room_id');
    }

//    protected static function boot() {
//        parent::boot();
//
//        static::deleting(function($room) { // before delete() method call this
//            $room->images()->delete();
//            // do the rest of the cleanup...
//        });
//    }

}
