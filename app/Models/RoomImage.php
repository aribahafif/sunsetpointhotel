<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RoomImage extends Model
{
    protected $fillable = [
        'room_id',
        'src'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
    public function getUrlAttribute()
    {
        return Storage::url("rooms/{$this->src}");
    }
    public function getThumbsAttribute()
    {
        return Storage::url("rooms/thumbs/{$this->src}");
    }
}
