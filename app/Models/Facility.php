<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Facility extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image'
    ];

    /**
     * get image url attribute mutator
     *
     * @param $value
     * @return mixed
     */
    public function getImageAttribute($value)
    {
        return Storage::url("facilities/{$value}");
    }
}
