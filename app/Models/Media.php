<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * get url mutator attribute
     *
     * @return mixed
     */
    public function getUrlAttribute()
    {
        return Storage::url("media/{$this->name}");
    }

    /**
     * polymorphism relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function mediaable(){
    	return $this->morphTo();
    }
}
