<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RoomFacility extends Model
{
    /**
     * @var string
     */
    protected $table='room_facilities';

    /**
     * @var array
     */
    protected $fillable=[
    	'name',
    	'image',
    	'description'
    ];

    /**
     * get image url mutator attribute
     *
     * @param $value
     * @return mixed
     */
    public function getImageAttribute($value)
    {
        return Storage::url("room_facilities/{$value}");
    }

    /**
     * relationship many to many between room facility and room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function room(){
    	return $this->belongsToMany(Room::class, 'room_facility_pivot');
    }
}
