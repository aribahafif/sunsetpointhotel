<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('room_facility_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('room_id')->index();
            $table->unsignedInteger('room_facility_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_facilities');
        Schema::dropIfExists('room_facility_pivot');
    }
}
