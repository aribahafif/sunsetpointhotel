<?php

use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Testimony Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the testimoni factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(\App\Models\Testimony::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $status = ['published', 'unpublished'];
    return [
        'name' => $faker->name,
        'content' => join("\n\n", $faker->sentences(mt_rand(3,6))),
        'status' => $status[mt_rand(0,1)]
    ];
});
