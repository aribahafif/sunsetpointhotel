<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Facility Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the facility factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Facility::class, function (Faker $faker) {
    $images = dirToArray(storage_path('/app/public/facilities'));
    $status = ['published', 'unpublished'];

    return [
        'title' => $faker->word,
        'description' => $faker->sentence,
        'image' => $images[mt_rand(0,9)],
        'status' => $status[mt_rand(0,1)]
    ];
});
