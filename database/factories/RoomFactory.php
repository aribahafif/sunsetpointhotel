<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Room Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the room factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(\App\Models\Room::class, function (Faker $faker) {
    $status = ['published', 'unpublished'];
    $markdown = new Parsedown();

    return [
        'name' => $faker->word,
        'price' => $faker->randomNumber,
        'detail' => $markdown->text(join("\n\n", $faker->paragraphs(mt_rand(3,6)))),
        'status' => 'published'
    ];
});

$factory->define(\App\Models\RoomFacility::class, function(Faker $faker) {
	$images = dirToArray(storage_path('/app/public/room_facilities'));
	return [
		'image' => $images[mt_rand(0,10)],
		'name' => $faker->word,
	];
});

$factory->define(\App\Models\RoomImage::class, function(Faker $faker) {
    $images = dirToArray(storage_path('/app/public/rooms'));
    return [
        'src' => $images[mt_rand(0,10)]
    ];
});

