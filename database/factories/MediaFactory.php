<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Media::class, function (Faker $faker) {
	$images = dirToArray(storage_path('/app/public/media'));
    return [
        'name' => $images[mt_rand(1,9)]
    ];
});
