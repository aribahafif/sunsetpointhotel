<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Users Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the user factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    return [
        'name' => $faker->name,
        'email' => 'admin@gmail.com',
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
