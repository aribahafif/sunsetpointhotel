<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(\App\Models\User::class, 1)->create();
        factory(\App\Models\Facility::class, 20)->create();
        factory(\App\Models\Testimony::class, 10)->create();
        factory(\App\Models\Media::class, 20)->create();

        factory(\App\Models\Room::class, 6)->create()->each(function ($u) {
            $u->room_facilities()->saveMany(factory(\App\Models\RoomFacility::class, 4)->make());
            $u->images()->saveMany(factory(\App\Models\RoomImage::class, 10)->make());
        });
    }
}
